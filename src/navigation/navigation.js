

import * as React from 'react';
import { Platform } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SigninScreen from '../Screens/Signin/Signin.component'
import SignUpScreen from '../Screens/Signup/signup.component'
import ResetpasswordScreen from '../Screens/ResetPassword/resetpassword.component'
import UsernameScreen from '../Screens/Username/username.component'
import Enteremail from '../Screens/ResetPassword/enteremail.component'
import OtpScreen from '../Screens/ResetPassword/Optscreen.component'
import Card from '../components/card'




const Stack = createStackNavigator();

function LoginNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="SignIn" 
        component={SigninScreen} 
        options={{headerShown:false}}
    //     options={({ navigation }) => ({ 
    //       headerTitleStyle: {
    //         fontWeight: 'bold',
    //       },
    //       headerRight:()=> ( 
    //         <HeaderButtons HeaderButtonComponent={HeaderButton}>
    //        <Item title='Cart' iconName={Platform.OS === 'android' ? 'md-cart' :'ios-cart'} onPress= {()=>{navigation.navigate(CartScreen)}}/> 
    //      </HeaderButtons>
    //      ),
    //      headerLeft:()=> ( 
    //       <HeaderButtons HeaderButtonComponent={HeaderButton}>
    //      <Item title='Cart' iconName={Platform.OS === 'android' ? 'md-menu' :'ios-menu'} onPress= {()=>{navigation.toggleDrawer()}}/> 
    //    </HeaderButtons>
    //    ),
    //     })}
         />
         <Stack.Screen name="SignUpScreen" component={SignUpScreen} 
          options={{headerShown:false}}
          />
         <Stack.Screen name="Username" component={UsernameScreen} options={{headerTitle:false}} />
         <Stack.Screen name="ResetPassword" options={{headerTitle:false}} component={ResetpasswordScreen} />
         <Stack.Screen name="EnterEmail" options={{headerTitle:false}} component={Enteremail} />
         <Stack.Screen name="OtpScreen" options={{headerTitle:false}} component={OtpScreen} />
         <Stack.Screen name="Card" options={{headerTitle:false}} component={Card} />
      </Stack.Navigator>
      </NavigationContainer>
  
  );
}

// const orderNavigator = createStackNavigator();
// function orderNavigation (){
//   return (
  
//   <orderNavigator.Navigator>
//   <orderNavigator.Screen  name="OrderScreen" component={OrderScreen} 
//   options={({ navigation }) => ({
    
//     headerTitle:'Your Orders',
//     headerStyle: {
//       backgroundColor: Colors.primary,
//     },    
//     headerTitleStyle: {
//       fontWeight: 'bold',
//     },
//     headerLeft:()=> ( 
//       <HeaderButtons HeaderButton Component={HeaderButton}>
//      <Item title='Cart' iconName={Platform.OS === 'android' ? 'md-menu' :'ios-menu'} onPress= {()=>{navigation.toggleDrawer()}}/> 
//    </HeaderButtons>
//    ),
//   })} 
  
//   />
// </orderNavigator.Navigator>

    
//   )
// }

// const AdminNavigator = createStackNavigator();
// function AdNavigation (){
//   return (

//   <AdminNavigator.Navigator>
//   <AdminNavigator.Screen  name="UserProduct" component={AdminScreen} 
//   options={({ navigation }) => ({
    
//     headerTitle:'Your Products',
//     headerStyle: {
//       backgroundColor: Colors.primary,
//     },    
//     headerTitleStyle: {
//       fontWeight: 'bold',
//     }
//   })} 
  
//   />
// </AdminNavigator.Navigator>
//   )
// }



// const Drawer = createDrawerNavigator();

// function MyDrawer() {
//   return (
//     <NavigationContainer>
//     <Drawer.Navigator>
//        <Drawer.Screen name="productScreen" component={ProductView} />
//       <Drawer.Screen name="orderScreen" component={orderNavigation} />
//       <Drawer.Screen name="UserProduct" component={AdNavigation} />

    
     
//     </Drawer.Navigator>
//     </NavigationContainer>
//   );
// }

export default LoginNavigation;