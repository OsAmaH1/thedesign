import React , {useState} from 'react'
 import {View ,StyleSheet , ImageBackground,Text , Alert } from 'react-native'
import { TextInput,Button  } from 'react-native-paper';


const LoginScreen = props =>{
    const image = { uri: "https://pngtree.com/freebackground/geometric-frame-simple-poster-background_968073.html" };
    const [userName, setUsername ] = useState()
    const [validName , isSetValid] = useState(true)
    
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const  userEmailHandler = text =>{
    if(reg.test(text) === false){
      isSetValid(false)
      return false   
    }
    else{
      isSetValid(true)
      return true
    }
    setUsername(text)
   }


   const contnueHandler = ()=>{
   
      {props.navigation.navigate('OtpScreen')}
     }
     
   
  
return(
   
    
<View style={styles.Screen}> 
<ImageBackground source={image} style={styles.image}>



<View style={styles.terms}>
      <Text style={{ textAlign:'center' , width:'70%', fontSize:30}}>Wellcome!</Text>
      <Text style={{ textAlign:'center' , width:'35%' , fontSize:15, marginTop:8}}>Hope You Enjoy The Stay</Text>
      </View>

  <View style={styles.inputs}>
    <TextInput style={styles.Text}
    label="Email"
    mode="outlined"
    value={userName}
    blurOnSubmit
    keyboardType="email-address"
    onChangeText={userEmailHandler}
  />
  {!validName && <Text style={{color:'red'}}>must enter a valid email!</Text>}
 
  {/* {props.navigation.navigate('Username')} */}
  <Button style={styles.button}  color="#00C9FF"  mode="contained" onPress={contnueHandler}>
   Continue
  </Button>
      </View> 

   

   
  
</ImageBackground>
   
 
  </View>
   

    
);
}

const styles= StyleSheet.create({
    Screen:{
        width:"100%",
        flex:1,
        justifyContent:'center'
        
        
      
    },
    image: {
         flex:1,
         height:'100%',
        resizeMode: "cover",
        justifyContent: "center"
      },
      Text:{
        
        marginHorizontal:20,
        padding:10,
        height:50,
        marginTop:5,
        borderRadius:10
       
    },
   
    button:{
        marginHorizontal:25,
        padding:7,
        width:'85%',
        marginLeft:30,
        borderRadius:10
  
    },
    signup:{
      marginHorizontal:25,
        padding:7,
        width:'85%',
        marginLeft:30,
        marginTop:30,
        borderRadius:10

    },
    terms:{
      alignItems:'center',
    },
    inputs:{
    marginTop:100
    }
})

export default LoginScreen;