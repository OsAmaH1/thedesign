 import React, {useState, useRef, useEffect } from 'react'
 import {TextInput,View ,StyleSheet , ImageBackground , Text, Alert , ScrollView ,TouchableOpacity,Dimensions, PixelRatio} from 'react-native'
import {  Button  } from 'react-native-paper';
import DatePicker from 'react-native-datepicker';
import {Picker} from '@react-native-community/picker';
import Icon from 'react-native-vector-icons/Ionicons'

// import RNPickerSelect from '@react-native-community/picker';




const SignUpScreen = props =>{
    const image = { uri: "https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages.unsplash.com%2Fphoto-1535332371349-a5d229f49cb5%3Fixlib%3Drb-1.2.1%26w%3D1000%26q%3D80&imgrefurl=https%3A%2F%2Funsplash.com%2Fimages%2Fnature%2Fmoon&tbnid=0XaCaiOPy526DM&vet=12ahUKEwjsl7Xhyb7sAhXD44UKHcreDVUQMygOegUIARDqAQ..i&docid=fqegj2N8n_8jqM&w=1000&h=1250&q=images&ved=2ahUKEwjsl7Xhyb7sAhXD44UKHcreDVUQMygOegUIARDqAQ" };

   
   const [firstName, setFirstName]  = useState()
   const [validfName, isValidFname] = useState(true)
   const [lastName, setLastName]  = useState()
   const [validlName, isValidLname] = useState(true)
   const [date, setDate]  = useState()
   const [validDate, isValidDate] = useState(true)
   const [dropDownValue, setDropDownValue]  = useState()
   const [validDvalue, isValidDvalue] = useState(true)
   const [country, setCountry]  = useState() 
   const [validCountry, isValidCountry] = useState(true)
   const [password, setPassword]  = useState()
   const [validPassword, isValidPassword] = useState(true)
   const [reenterpassword, setreenterpassword] = useState()
   const [email, setEmail]  = useState()
   const [validEmail, isValidEmail] = useState(true)
   const [flag , setFlag] = useState(true)
   const [passflag , setpassFlag] = useState(true)
   const [buttonWidth, setButtonWidth] = useState(Dimensions.get('window').width * 0.85)
   


   useEffect(()=>{
    const updateLayout = ()=>{
      setButtonWidth(Dimensions.get('window').width * 0.85)
    }
    Dimensions.addEventListener('change', updateLayout);
    return()=>{
      Dimensions.removeEventListener('change',updateLayout);
    }
  })

 


     
   const  firstNameHandler = (text) => {
        if(!text){
       isValidFname(false)
       return false
     }else{
       isValidFname(true)
       return true
     }
 }

   function lastNameHandler (text) {
    if(!text){
      isValidLname(false)
      return false
    }else{
      isValidLname(true)
      return true 
    }
    
  } 
  function dateHandler(text){
    if(!text){
      isValidDate(false)
      return false
    }
    else {
      isValidDate(true)
      return true
    }
  }
  
  function dropDownHandler (text) {
    if(!text){
      isValidDvalue(false)
      return false
    }else{
      isValidDvalue(true)
      return true 
    }
    
  } 
  
  
  function countryHandler (text) {
    if(!text){
      isValidCountry(false)
      return false
    }else{
      isValidCountry(true)
      return true 
    }
   
  } 
  
  function passwordHandler(text) {
    if(!text){
      isValidPassword(true)
      return true
       
    }else{
      isValidPassword(false)
      return false
    }
  
  } 
  const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  
 const emailHandler =(text) => { 
    if(reg.test(text) === false){
      isValidEmail(false)
      return false
    }else{
      isValidEmail(true)
      return true 
    }
    
  }
  const passwordToggleHandler =()=>{
    setFlag(!flag);
  }

  const repasswordToggleHandler =()=>{
    setpassFlag(!passflag);
  }
  
  

  function nextButtonHandler(fname,lname,date,dropDown,country,email,pass,repass){
     console.log('hi there')
    let checkFName = firstNameHandler(fname)
    let checkLName = lastNameHandler(lname)
    let checkDate = dateHandler(date)
    let checkDropDown = dropDownHandler(dropDown)
    let checkCountry = countryHandler(country)
    let checkEmail = emailHandler(email)
    let checkPassword = passwordHandler(pass)
    let checkRepassword = passwordHandler(repass)
    if (checkFName && checkLName &&  checkDate &&  checkDropDown &&  checkCountry && checkEmail)
    {
      props.navigation.navigate('Username',{
        firstname:firstName,
        lastname:lastName,
        date:date,
        dropDown:dropDownValue,
        country:country,
        email:email,
        password:password
 })
    }
    }



  const datePickerRef = useRef(null);

  const ref_input2 = useRef();
  const ref_input3 = useRef();
  const ref_input4 = useRef();
  const ref_input5 = useRef();
  const ref_input6 = useRef();
  const ref_input7 = useRef();
  const ref_input8 = useRef();
  // const biggerScreen = PixelRatio.roundToNearestPixel(50)

  const givendeviceWidth = Dimensions.get('window').width;
  const givendeviceHeight = Dimensions.get('window').height;

  const layOut = givendeviceWidth < 320 ? 30:50;
  const biggerScreen = givendeviceWidth > 580 ? 55 : layOut;
  
  const fontSize = givendeviceWidth < 480 ? 10 : 15;

  const margin = givendeviceHeight < 481 ? 30 :80;
  
  const size = givendeviceWidth < 600 ?  PixelRatio.getPixelSizeForLayoutSize(15):  PixelRatio.roundToNearestPixel(25);
  const subsize = givendeviceWidth < 600 ?  PixelRatio.roundToNearestPixel(12) :  PixelRatio.roundToNearestPixel(20) ;
 

return(
  <ScrollView>
<View style={styles.Screen}> 
<ImageBackground source={image} style={styles.image}>
<View style={{width:'100%' , alignItems:'flex-end'}}>
  <Button style={{ width:"30%", justifyContent:'flex-end', marginTop:30, borderRadius:15, marginRight:28}}  color="#00C9FF"  mode="contained" onPress={() => {props.navigation.navigate('SignIn')}}>
   SignIn
  </Button>
      </View>
      <View style={styles.terms}>
      <Text style={{ textAlign:'center' , width:givendeviceWidth * 0.7, fontSize:size }}>Hello!</Text>
      <Text style={{ textAlign:'center' , width:givendeviceWidth * 0.35 , fontSize:subsize, marginTop:8, marginBottom:margin}}>Get Started free i few seconds</Text>
      </View>
<View style={{marginTop:60}}> 
<View style={[styles.firstrow, {width:buttonWidth}]}>
  <TextInput style={{width:givendeviceWidth * 0.49 , borderWidth:1 , height:biggerScreen, borderColor:'#19A5D3', width:"49%" , borderRadius:10, marginLeft:30}}
  borderColor='#00C9FF'
    value={firstName}
    placeholder="Name"
    onSubmitEditing={() => ref_input2.current.focus()}
    onChangeText={text=>setFirstName(text)}
    
  />
  
  <TextInput style={{width:givendeviceWidth * 0.49 , borderWidth:1 , height:biggerScreen, borderColor:'#19A5D3', paddingHorizontal:16,marginLeft:5, width:"49%" , borderRadius:10, marginLeft:10}}
    placeholder="Last Name"
    returnKeyType = {"next"}
    autoFocus = {true}
     ref={ ref_input2}
    value={lastName}
    onSubmitEditing={() => ref_input3.current.focus()}
    onChangeText={text=>setLastName(text)}
    
   
  />
 
  </View> 
  <View style={{width:givendeviceWidth , flexDirection:'row'}}>
  <View style={{width:givendeviceWidth / 2}}>
  {!validfName && <Text style={{color:'red',   marginLeft:30}}>must input your firstname</Text>}
  </View>
  <View style={{width:givendeviceWidth /2 }}> 
    {validlName ? null : <Text style={{color:'red' ,  marginLeft:5 }}>must input your lastname</Text>}
    </View>
    </View>
 
 
  <View style={{flexDirection:'row', width:givendeviceWidth * 0.89, justifyContent:'space-around', marginBottom:5 , marginHorizontal:20}}>
   <View style={{width:givendeviceWidth * 0.42, borderWidth:1 , height:biggerScreen, borderColor:'#19A5D3' , borderRadius:10 , flexDirection:'row' , marginLeft:2}}>
      
       <TextInput style={{width:givendeviceWidth * 0.35, justifyContent:'center'}}
         returnKeyType = {"next"}
         autoFocus = {true}
        ref={ref_input3}
        onSubmit={() => ref_input4.current.focus()}
        placeholder="D-O-B"
        value={date}
        /> 
        <TouchableOpacity onPress={() => datePickerRef.current.onPressDate()}  style={{ width:givendeviceWidth * 0.10 , justifyContent:'center'}}>
       <Icon  name="calendar" size={20}  color='red'/>
      </TouchableOpacity>
    
  <DatePicker              // Initial date from state
          mode="date" // The enum of date, datetime and time
          showIcon={false}
           hideText={true}
           ref={datePickerRef}
          placeholder="D-O-B"
          format="DD-MM-YYYY"
          minDate="01-01-2016"
          maxDate="01-01-2050"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              //display: 'none',
              position: 'absolute',
              right: 0,
              top: 4,
              marginLeft: 0,
            },
           
          }}
          
          onDateChange={text=>setDate(text)}
        />
        </View>
         
      <View style={{width:givendeviceWidth * 0.41, borderWidth:1 , height:biggerScreen, borderColor:'#19A5D3' , borderRadius:10, justifyContent:'center'}}>
        <Picker
        mode='dropdown'
        returnKeyType = {"next"}
        autoFocus = {true}
         ref={ ref_input4}
         onSubmitEditing={() => ref_input5.current.focus()}
          selectedValue={dropDownValue}
          onValueChange={(itemValue) =>
            setDropDownValue(itemValue)}>
          <Picker.Item label="Gender" value="" />
          <Picker.Item label="Male" value="Male" />
          <Picker.Item label="Female" value="Female"/>
      </Picker>
      </View>
     
     
  </View>
  <View style={{width:givendeviceWidth , flexDirection:'row'}}>
   <View style={{width:givendeviceWidth * 0.49}}>
     {!validDate && <Text style={{color:'red', width:givendeviceWidth ,  marginLeft:30}}>must choose valid date</Text>}
     </View> 
   <View style={{width:givendeviceWidth * 0.49}}>
      {!validDvalue && <Text style={{color:'red', width:givendeviceWidth,  marginLeft:5}}>must choose one option</Text>}
  </View> 
  
  </View>
  
  
  <View>
   <TextInput style={{width:buttonWidth,borderWidth:1 , height:biggerScreen, borderColor:'#19A5D3', borderRadius:10,marginHorizontal:30}}
    value={country}
    returnKeyType = {"next"}
    autoFocus = {true}
   ref={ ref_input5}
   onSubmitEditing={() => ref_input6.current.focus()}
    placeholder="Country" 
    onChangeText={text=>setCountry(text)}
  />
  {!validCountry && <Text style={{color:'red', width:givendeviceWidth , marginLeft:30}}>must input your Country name</Text>}
  </View>
  





  <View style={{ width:buttonWidth, borderWidth:1 , height:biggerScreen, borderColor:'#19A5D3', borderRadius:10,marginHorizontal:30, marginTop:5}}>
  <TextInput style={styles.text}
  value={email}
   placeholder="Email"
   returnKeyType = {"next"}
   autoFocus = {true}
  ref={ ref_input6}
  onSubmitEditing={() => ref_input7.current.focus()}
   onChangeText={(text)=>setEmail(text)}

  />
  
  </View>
  {!validEmail && <Text style={{color:'red', width:givendeviceWidth, marginLeft:30}}>must input your Valid Email Address</Text>}
  
  
  





   <View style={{width:buttonWidth, marginHorizontal:30 , justifyContent:'center'}}>
   <View style={{borderWidth:1 , height:biggerScreen, borderColor:'#19A5D3' , borderRadius:10,  marginTop:5, flexDirection:'row'}}>
     <TextInput style={{width: givendeviceWidth *0.75}}
     value={password}
     onChangeText={text=>setPassword(text)}
    placeholder="password*"
    returnKeyType = {"next"}
    autoFocus = {true}
   ref={ ref_input7}
   onSubmitEditing={() => ref_input8.current.focus()}
     secureTextEntry={flag}
  /> 
  <TouchableOpacity onPress={passwordToggleHandler}  style={{ width:'10%', justifyContent:'center'}}>
  <Icon  name={flag === true ? "eye-off" :'eye'} size={20}  />
 </TouchableOpacity>
  </View>
  {!validPassword && <Text style={{color:'red', width:'100%'}}>must input your atleast 8 chracter password</Text>}


  <View style={{borderWidth:1 , height:biggerScreen, borderColor:'#19A5D3',  borderRadius:10, marginTop:5}}>
  <View style={{flexDirection:'row'}}>
  <TextInput style={{width:givendeviceWidth *0.75}}
  value={reenterpassword}
  onChangeText={text=>setreenterpassword(text)}
    placeholder="Re-enter Password*"
    returnKeyType = {"next"}
    autoFocus = {true}
   ref={ ref_input8}
    secureTextEntry={passflag}
  />
   <TouchableOpacity onPress={repasswordToggleHandler}  style={{ width:givendeviceWidth * 0.10,justifyContent:'center'}}>
  <Icon  name={passflag === true ? "eye-off" :'eye'} size={20}  />
 </TouchableOpacity>
 </View>

  </View>
  {!validPassword && <Text style={{color:'red', width:givendeviceWidth}}>must input your atleast 8 chracter re-password</Text>}  
  </View>
  <View>
  <Button style={[styles.button, {width:buttonWidth, height:biggerScreen}]}  color="#00C9FF"  mode="contained" onPress={() => {nextButtonHandler(firstName,lastName,date,dropDownValue,country,email,password)}}>
   Next
  </Button>
      </View> 
</View>
  
</ImageBackground>
   
 
  </View>   
  </ScrollView>
);
}

const styles= StyleSheet.create({
    Screen:{
        width:"100%",
        flex:1,
        justifyContent:'center',
    },
    image: {
         flex:1,
         height:'100%',
        resizeMode: "cover",
        justifyContent: "center"
      },
    firstrow:{
        flexDirection:'row',
        alignItems:'center',
        marginBottom:5
        
      
    },
    firstText:{
        width:'49%',
        justifyContent:'center',
        height:50,
        marginTop:5
    },
    button:{
        marginHorizontal:25,
        padding:7,
        marginLeft:30,
        marginTop:25,
        marginBottom:20,
        justifyContent:'center'

    },
    terms:{
        alignItems:'center',
        marginTop:20
      },
      datePickerStyle: {
        width:'49%',
        marginTop:20,
        borderRadius:10,
        
       
      },
})

export default SignUpScreen;