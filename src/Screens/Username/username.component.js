import React from 'react'
 import {View ,StyleSheet , ImageBackground,Text,PixelRatio } from 'react-native'
import { TextInput,Button  } from 'react-native-paper';


const UsernameScreen = ({ props,route }) =>{
    const image = { uri: "https://pngtree.com/freebackground/geometric-frame-simple-poster-background_968073.html" };

    const {firstname,lastname,date, dropDown, country, email , password } = route.params;
    console.log(firstname);
    console.log(lastname);
    console.log(date);
    console.log(dropDown);
    console.log(country);
    console.log(email);
    console.log(password);

return(
   
    
<View style={styles.Screen}> 
    <ImageBackground source={image} style={styles.image}>
      

    <View style={{marginBottom:280}}>
    <View style={styles.terms}>
         <Text style={{ textAlign:'center' , width:'70%', fontSize:30}}>Username</Text>
         <Text style={{ textAlign:'center' , width:'50%' , fontSize:15, marginTop:8}}>Choose your unique Handle</Text>
      </View>
    </View>
      
      <View style={styles.inputs}>
         <TextInput style={styles.Text}
            label="username*"
            blurOnSubmit
            onChangeText={() => {}} />
     </View> 
     <View>
        <Button style={styles.button}  color="#00C9FF"  mode="contained" onPress={props.navaigation.navaigate('Card')}>
          Register
        </Button>
     </View> 

  
    </ImageBackground>
</View>
   

    
);
}

const styles= StyleSheet.create({
    Screen:{
        flex:1,
        width:"100%",
        justifyContent:'center'       
    },
    image: {
         flex:1,
         height:'100%',
        resizeMode: "cover",
        justifyContent: "center"
      },
      Text:{
        marginHorizontal:20,
        padding:10,
        height:50,
        marginTop:5,
        borderRadius:10
    },
   
    button:{
        marginHorizontal:25,
        padding:7,
        width:'85%',
        marginLeft:30,
        borderRadius:10,
        marginTop:40
    },
    signup:{
      marginHorizontal:25,
        padding:7,
        width:'85%',
        marginLeft:30,
        marginTop:30,
        borderRadius:10
    },
    terms:{
        
        alignItems:'center',
        justifyContent:'flex-start'
      },
   
})

export default UsernameScreen;