import React from 'react';
import { View , Text , StyleSheet , TouchableOpacity ,Dimensions} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const CardScreen = (props) =>{

    

 return(

    <View >
        <View style={{ height:'100%'}}>
        <TouchableOpacity onPress={() => {}}  style={{ marginRight:20, width:'15%' , justifyContent:'center'}}>
       <Icon  name="cart" size={20}  color='Black'/>
      </TouchableOpacity>
        <Image
        style={styles.logo}
        source={{
          uri:
            'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAzCAYAAAA6oTAqAAAAEXRFWHRTb2Z0d2FyZQBwbmdjcnVzaEB1SfMAAABQSURBVGje7dSxCQBACARB+2/ab8BEeQNhFi6WSYzYLYudDQYGBgYGBgYGBgYGBgYGBgZmcvDqYGBgmhivGQYGBgYGBgYGBgYGBgYGBgbmQw+P/eMrC5UTVAAAAABJRU5ErkJggg==',
        }}
         /> 
        </View>
        <View style={{flexDirection:'row', marginHorizontal:10}}>
    <Text style={{justifyContent:'flex-start'}}>Price $200</Text>    
    <Text style={{justifyContent:'flex-end'}}>Details</Text>

        </View>
    </View>

);
}


const styles = StyleSheet.create({
    logo: {
        width:'80%',
        height: 200,
        marginHorizontal:20
      }

});


export default CardScreen;