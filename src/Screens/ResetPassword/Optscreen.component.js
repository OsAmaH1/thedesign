import React , {useState} from 'react'
 import {View ,StyleSheet , ImageBackground,Text , Alert , TouchableOpacity} from 'react-native'
import { TextInput,Button  } from 'react-native-paper';


const LoginScreen = props =>{
    const image = { uri: "https://pngtree.com/freebackground/geometric-frame-simple-poster-background_968073.html" };
    const [userName, setUsername ] = useState()
    const [validName , isSetValid] = useState(true)
    
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const  userEmailHandler = text =>{
    if(reg.test(text) === false){
      isSetValid(false)
      return false   
    }
    else{
      isSetValid(true)
      return true
    }
    setUsername(text)
   }


   const emailHandler = ()=>{
    if(validName){
      {props.navigation.navigate('ResetPassword')}
     }
     
   }
  
return(
   
    
<View style={styles.Screen}> 
<ImageBackground source={image} style={styles.image}>



<View style={styles.terms}>
      <Text style={{ textAlign:'center' , width:'70%', fontSize:30}}>Wellcome!</Text>
      <Text style={{ textAlign:'center' , width:'35%' , fontSize:15, marginTop:8}}>Hope You Enjoy The Stay</Text>
      </View>

  <View style={styles.inputs}>
   <View style={{flexDirection:'row',justifyContent:'space-evenly',marginLeft:35, marginTop:110}}> 
   
   <TextInput style={styles.Text}
    mode="outlined"
    value={userName}
    blurOnSubmit
    onChangeText={userEmailHandler}
  />
   <TextInput style={styles.Text}
    mode="outlined"
    value={userName}
    blurOnSubmit
    onChangeText={userEmailHandler}
  />
   <TextInput style={styles.Text}
    mode="outlined"
    value={userName}
    blurOnSubmit
    onChangeText={userEmailHandler}
  />
    <TextInput style={styles.Text}
    mode="outlined"
    value={userName}
    blurOnSubmit
    onChangeText={userEmailHandler}
  />
    <TextInput style={styles.Text}
    mode="outlined"
    value={userName}
    blurOnSubmit
    onChangeText={userEmailHandler}
  />
    <TextInput style={styles.Text}
    mode="outlined"
    value={userName}
    blurOnSubmit
    onChangeText={userEmailHandler}
  />
  </View>  

  {/* {!validName && <Text style={{color:'red'}}>must enter a valid email!</Text>} */}
 
  {/* {props.navigation.navigate('Username')} */}
  <Button style={styles.button}  color="#00C9FF"  mode="contained" onPress={emailHandler}>
   Verify
  </Button>

  <TouchableOpacity onPress={() => {}}  style={{marginTop:20, width:'100%', marginLeft:20}}>
      <Text  style={{borderWidth:1 , height:50, borderColor:'#19A5D3', borderRadius:10, textAlign:'center',textAlignVertical:'center', color:'#19A5D3'}}>Resend Code</Text>
      </TouchableOpacity>
  {/* <Button style={styles.button}  color="white"  mode="contained" onPress={()=>{}}>
   Resend Code
  </Button> */}
      </View> 

   

   
  
</ImageBackground>
   
 
  </View>
   

    
);
}

const styles= StyleSheet.create({
    Screen:{
        width:"100%",
        flex:1,
        justifyContent:'center'
        
        
      
    },
    image: {
         flex:1,
         height:'100%',
        resizeMode: "cover",
        justifyContent: "center"
      },
      Text:{
        
        marginHorizontal:10,
        padding:10,
        height:50,
        marginTop:5,
        borderRadius:10,
        width:'21%'
       
    },
   
    button:{
        marginHorizontal:25,
        padding:7,
        width:'100%',
        marginLeft:20,
        borderRadius:10,
        marginTop:50,
        textDecorationColor:'white'
  
    },
   
    terms:{
      alignItems:'center',
    },
    inputs:{
    marginTop:100,
    width:'90%'
    }
})

export default LoginScreen;