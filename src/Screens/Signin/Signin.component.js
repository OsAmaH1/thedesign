import React , {useState, useEffect, useRef} from 'react'
 import {View ,StyleSheet , ImageBackground,Text , Alert, Dimensions, ScrollView , PixelRatio} from 'react-native'
import { TextInput,Button  } from 'react-native-paper';


const LoginScreen = props =>{
    const image = { uri: "https://pngtree.com/freebackground/geometric-frame-simple-poster-background_968073.html" };
    const [userName, setUsername ] = useState()
    const [validName , isSetValid] = useState(false)
    const [password , setPassword ] = useState()
    const [valid , isValid] = useState(false)
    const [buttonWidth, setButtonWidth] = useState(Dimensions.get('window').width * 0.85)



    
    // useEffect(()=>{
    //   const updateLayout = ()=>{
    //     setButtonWidth(Dimensions.get('window').width * 0.85)
    //   }
    //   Dimensions.addEventListener('change', updateLayout);
    //  return()=>{
    //    Dimensions.removeEventListener('change',updateLayout);
    //  }
    // })
   
  const  userNameHandler = text =>{
    if(text.length === 0){
      isSetValid(false)   
    }
    else{
      isSetValid(true)
    }
    setUsername(text)
   }

   const  passwordHandler = (text) =>{
     if(text.trim().length <= 6){
       isValid(false)   
     }
     else{
       isValid(true)
     }
     setPassword(text);
   } 
   const signInHandler = ()=>{
    if(validName && valid ){
      {props.navigation.navigate('Username')}
     }
     else{
      Alert.alert(
        'Ooops',
        'Please enter valid information',
        [
         
          {text: 'OK', style: 'cancel'},
        ],
        { 
          cancelable: true 
        }
      );
       
     }
   }

   const givendeviceWidth = Dimensions.get('window').width;
   const givendeviceHeight = Dimensions.get('window').height;
 
  //  const layOut = givendeviceWidth < 320 ? 30:50;
  //  const biggerScreen = givendeviceWidth > 600 ? 80 : layOut;
   
  //  const fontSize = givendeviceWidth < 480 ? 10 : 15;

  const widthOfButtons = givendeviceWidth > 580 ? givendeviceWidth * 0.90 : givendeviceWidth* 0.85;
 
   const layOut = givendeviceHeight < 481 ? 30 :50;
   const biggerScreen = givendeviceWidth > 601 ? 65 : layOut;
   
   const size = givendeviceWidth < 600 ?  PixelRatio.getPixelSizeForLayoutSize(12) :  PixelRatio.roundToNearestPixel(25);
   const subsize = givendeviceWidth < 600 ?  PixelRatio.roundToNearestPixel(10) :  PixelRatio.roundToNearestPixel(18) ;

   const ref_input2 = useRef();
   const ref_input3 = useRef();
  
return(
   
<ScrollView>
<View style={styles.Screen}> 
<ImageBackground source={image} style={styles.image}>

<View style={{marginTop:50}}>

<View style={styles.terms}>
      <Text style={{ textAlign:'center' , width:'70%', fontSize:size}}>Wellcome!</Text>
      <Text style={{ textAlign:'center' , width:'35%' , fontSize:subsize, marginTop:8}}>Hope You Enjoy The Stay</Text>
      </View>

  <View style={styles.inputs}>
    <TextInput style={[styles.Text, {height:biggerScreen}]}
    label="Username or Email"
    mode="outlined"
    autoFocus={true}
    returnKeyType="next"
    onSubmitEditing={() => ref_input2.current.focus()}
    keyboardType="email-address"
    onChangeText={userNameHandler}
  />
  {/* {!validName && <Text style={{color:'red'}}>wrong username or email!</Text>} */}
  <TextInput style={[styles.Text, , {height:biggerScreen}]}
    label="Pasword"
    mode="outlined"
    textContentType="password"
    value={password}
    ref={ref_input2}
    onChangeText={passwordHandler}

  />
  {/* {!valid && <Text>wrong or incomplete Password!</Text>} */}
  <Text style={{textAlign:'right' , width:'92%' , marginBottom:20}} onPress={() =>{props.navigation.navigate('EnterEmail')}}>Forgot Password?</Text>
  </View> 
    
  <View>
  {/* {props.navigation.navigate('Username')} */}
  <Button style={{
     marginHorizontal:25,
     padding:7,
     width:widthOfButtons,
     height:biggerScreen,
     justifyContent:'center',
     borderRadius:10
  }}  color="#00C9FF"  mode="contained" onPress={signInHandler}>
   Sign In
  </Button>
      </View> 

      <View>
      <Text style={{textAlign:'center' , width:'100%' , marginTop:40,  marginBottom:30,}}>Don't Have an account?</Text>
      </View> 

      <View>
  <Button 
  style={{width:widthOfButtons, 
        marginHorizontal:25,
        padding:7,
        height:biggerScreen,
        justifyContent:'center',
        marginTop:30,
        borderRadius:10}}  
        color="black"  mode="contained" onPress={()=>{props.navigation.navigate('SignUpScreen')}}>
   Sign Up
  </Button>

      </View> 
      </View>

      <View style={styles.last}>
      <Text style={{ textAlign:'center' , width:'70%' , marginTop:10, opacity:0.35, marginBottom:20}}>You agree to the terms and conditions and are of legal age above 16 </Text>
      </View>

  
</ImageBackground>
   
 
  </View>
  </ScrollView>  

    
);
}

const styles= StyleSheet.create({
    Screen:{
        width:"100%",
        flex:1,
        justifyContent:'center'
        
        
      
    },
    image: {
         flex:1,
         height:'100%',
        resizeMode: "cover",
        justifyContent: "center"
      },
      Text:{
       
        marginHorizontal:20,
        padding:10,
        
        marginTop:5,
        borderRadius:10
       
    },
   
    // button:{
    //     marginHorizontal:25,
    //     padding:7,
    //     width:'85%',
    //     marginLeft:30,
    //     borderRadius:10
  
    // },
    // signup:{
    //   marginHorizontal:25,
    //     padding:7,
    //     marginLeft:30,
    //     marginTop:30,
    //     borderRadius:10

    // },
    terms:{
      alignItems:'center',
      marginTop:100
    },
    last:{
      alignItems:'center',
      marginTop:30
    },
    inputs:
    {
    marginTop:100
    }
})

export default LoginScreen;