import React from 'react'
 import {View ,StyleSheet , ImageBackground,Text } from 'react-native'
import { TextInput,Button  } from 'react-native-paper';


const LoginScreen = props =>{
    const image = { uri: "https://pngtree.com/freebackground/geometric-frame-simple-poster-background_968073.html" };
return(
   
    
<View style={styles.Screen}> 
<ImageBackground source={image} style={styles.image}>
  <View style={{marginTop:80}}>
<View style={styles.terms}>
      <Text style={{ textAlign:'center' , width:'70%', fontSize:30}}>Enter New Password</Text>
      <Text style={{ textAlign:'center' , width:'70%' , fontSize:15, marginTop:8}}>Please enter your new password below,
       a strong password adds more security</Text>
      </View>
  <View style={styles.inputs}>
     <TextInput style={styles.Text}
            label="Enter Password"
            mode="outlined"
            blurOnSubmit
            onChangeText={() => {}} />
     <TextInput style={styles.Text}
            label="Re-enter Pasword"
            mode="outlined"
            onChangeText={() => {}}/>
  </View> 
  </View>
  <View>
      <Button style={styles.button}  color="#00C9FF"  mode="contained" onPress={() => console.log('Pressed')}>
            Save
      </Button>
  </View>
</ImageBackground>
  </View>    
);
}

const styles= StyleSheet.create({
    Screen:{
        width:"100%",
        flex:1,
        justifyContent:'center'
        
        
      
    },
    image: {
         flex:1,
         height:'100%',
        resizeMode: "cover",
        justifyContent: "center"
      },
      Text:{
        
        marginHorizontal:20,
        padding:10,
        height:50,
        marginTop:5,
        borderRadius:10
       
    },
   
    button:{
        marginHorizontal:25,
        padding:7,
        width:'85%',
        marginLeft:30,
        borderRadius:10,
        marginTop:100
  
    },
    signup:{
      marginHorizontal:25,
        padding:7,
        width:'85%',
        marginLeft:30,
        marginTop:30,
        borderRadius:10

    },
    terms:{
      alignItems:'center',
    },
    inputs:{
    marginTop:100
    }
})

export default LoginScreen;